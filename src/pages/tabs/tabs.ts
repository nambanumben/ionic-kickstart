import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {IonicPage, NavController} from 'ionic-angular';

import {Tab1Root} from '../pages';
import {Tab2Root} from '../pages';
import {Tab3Root} from '../pages';
import {BaseSecurePage} from "../../providers/auth/secure-page";
import {AuthService} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage extends BaseSecurePage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";

  constructor(public navCtrl: NavController,
              public translateService: TranslateService,
              public auth: AuthService) {
    super(auth);
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE']).subscribe(values => {
      this.tab1Title = values['TAB1_TITLE'];
      this.tab2Title = values['TAB2_TITLE'];
      this.tab3Title = values['TAB3_TITLE'];
    });
  }
}
