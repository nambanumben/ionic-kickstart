import {AuthService} from "./auth";

export class BaseSecurePage {
  constructor(public auth: AuthService) {
  }

  ionViewCanEnter() {
    return this.auth.authenticated;
  }
}
